# Scheduler 

## Installation

### Prerequisites

* To run this project, you must have PHP 7 installed.
* You should setup a host on your web server for your local domain. For this you could also configure Laravel Homestead or Valet. 

### Step 1

Begin by cloning this repository to your machine, and installing all Composer & NPM dependencies.

```bash
git clone https://Arpit_Yadav@bitbucket.org/arpit_yadav/scheduler.git
cd scheduler && composer install && npm install
cp .env.example .env (On Linux) copy .env.example .env (on windows)
(Upon opening .env, set)

APP_NAME=scheduler
MIX_APP_NAME=scheduler

(set these according to your configuration)
DB_DATABASE=
DB_USERNAME=
DB_PASSWORD=

php artisan key:generate
php artisan migrate
npm run dev
```

### Step 2

Next, boot up a server and visit the project. If using a tool like Laravel Valet, of course the URL will default to `http://scheduler.test`. 
or 
you can just ```php artisan serve```